#!/bin/bash

# 1. Imprime uma frase motivacional
echo "Nunca desista dos seus sonhos! Você é capaz de alcançar o que deseja."

# 2. Dá bom dia e boas-vindas ao usuário atual
echo "Bom dia e bem-vindo, $USER!"

# 3. Informa qual é o dia da semana
dia_semana=$(date +%A)
echo "Hoje é $dia_semana."

# 4. Informa como está o clima em João Pessoa
clima=$(curl wttr.in/JoaoPessoa?format=%C+%t)
echo "O clima em João Pessoa é: $clima."

